
-- =====================================================================

-- https://stackoverflow.com/a/14425862

function IsUnix()
  return package.config:sub(1, 1) == '/'
end

function IsWindows()
  return package.config:sub(1, 1) == '\\'
end

-- =====================================================================

if #arg == 1 then
  local LCmd
  
  if IsWindows() then
    LCmd = string.format('if exist %s sumatra %s', arg[1], arg[1])
  else
    LCmd = string.format('[ -f %s ] && (xdg-open %s > /dev/null 2>&1) &', arg[1], arg[1])
  end
  
  if not os.execute(LCmd) then
    io.write('Cannot open document\n')
  end
else
  io.write('Usage: lua view.lua FILE\n')
end
