%~ \documentclass[a4paper,twocolumn,french]{article}
\documentclass[a4paper,12pt]{article}

\usepackage{fontspec}
\usepackage[french]{babel}
\usepackage{microtype}
%~ \usepackage{polyglossia}
%~ \setdefaultlanguage{french}

\usepackage[margin=16mm,bmargin=24mm]{geometry}

%~ \setmainfont
%~ [
  %~ Path       = libre-baskerville/,
  %~ Extension  = .otf,
  %~ Ligatures  = TeX,
  %~ BoldFont   = LibreBaskerville-Bold,
  %~ ItalicFont = LibreBaskerville-Italic
%~ ]{LibreBaskerville-Regular}

%~ https://www.fontsquirrel.com/fonts/libre-baskerville

%~ \setmainfont{LibreBaskerville}

\setmainfont{EBGaramond}
[
  Extension      = .otf ,
  UprightFont    = *-Regular,
  ItalicFont     = *-Italic,
  BoldFont       = *-Bold,
  BoldItalicFont = *-BoldItalic,
  Numbers        = Lowercase,
  Ligatures      = Discretionary,
  Style          = Swash
]
% https://texnique.fr/osqa/questions/8423/lualatex-et-ligatures

\setlength{\columnsep}{8mm} 

\usepackage{lastpage}
\usepackage{fancyhdr}
\pagestyle{fancy}
\fancyhf{}
\fancyfoot[C]{\thepage \hspace{1pt} / \pageref{LastPage}}
\renewcommand{\headrulewidth}[0]{0pt}

\begin{document}

%~ \thispagestyle{fancy}
\thispagestyle{empty}

\begin{center}
\Large Les causes de la révolution d'Angleterre.
\end{center}

J'ai déjà dit quelque chose de la licence où se jettent les esprits quand on ébranle les fondements de la religion et qu'on remue les bornes une fois posées. Mais, comme la matière que je traite me fournit un exemple manifeste et unique dans tous les siècles de ces extrémités furieuses, il est, Messieurs, de la nécessité de mon sujet de remonter jusques au principe, et de vous conduire pas à pas par tous les excès où le mépris de la religion ancienne, et celui de l'autorité de l'Église, ont été capables de pousser les hommes.

Donc la source de tout le mal est que ceux qui n'ont pas craint de tenter au siècle passé la réformation par le schisme, ne trouvant point de plus fort rempart contre toutes leurs nouveautés que la sainte autorité de l'Église, ils ont été obligés de la renverser. Ainsi les décrets des conciles, la doctrine des Pères, et leur sainte unanimité, l'ancienne tradition du Saint-Siège et de l'Église catholique n'ont plus été comme autrefois des lois sacrées et inviolables. Chacun s'est fait à soi-même un tribunal où il s'est rendu l'arbitre de sa croyance ; et, encore qu'il semble que les novateurs aient voulu retenir les esprits en les renfermant dans les limites de l'Écriture sainte, comme ce n'a été qu'à condition que chaque fidèle en deviendrait l'interprète et croirait que le Saint-Esprit lui en dicte l'explication, il n'y a point de particulier qui ne se voie autorisé par cette doctrine à adorer ses inventions, à consacrer ses erreurs, à appeler Dieu tout ce qu'il pense. Dès lors on a bien prévu que, la licence n'ayant plus de frein, les sectes se multiplieraient jusqu'à l'infini ; que l'opiniâtreté serait invincible ; et que, tandis que les uns ne cesseraient de disputer, ou donneraient leurs rêveries pour inspirations, les autres, fatigués de tant de folles visions, et ne pouvant plus reconnaître la majesté de la religion déchirée par tant de sectes, iraient enfin chercher un repos funeste, et une entière indépendance, dans l'indifférence des religions, ou dans l'athéisme.

Tels, et plus pernicieux encore, comme vous verrez dans la suite, sont les effets naturels de cette nouvelle doctrine. Mais de même qu'une eau débordée ne fait pas partout les mêmes ravages, parce que sa rapidité ne trouve pas partout les mêmes penchants et les mêmes ouvertures, ainsi, quoique cet esprit d'indocilité et d'indépendance soit également répandu dans toutes les hérésies de ces derniers siècles, il n'a pas produit universellement les mêmes effets : il a reçu diverses limites, suivant que la crainte, ou les intérêts, ou l'humeur des particuliers et des nations, ou enfin la puissance divine, qui donne quand il lui plaît des bornes secrètes aux passions des hommes les plus emportés, l'ont différemment soutenu. Que s'il s'est montré tout entier à l'Angleterre, et si sa malignité s'y est déclarée sans réserve, les rois en ont souffert, mais aussi les rois en ont été cause. Ils ont trop fait sentir aux peuples que l'ancienne religion se pouvait changer. Les sujets ont cessé d'en révérer les maximes, quand ils les ont vu céder aux passions et aux intérêts de leurs princes. Ces terres trop remuées, et devenues incapables de consistance, sont tombées de toutes parts, et n'ont fait voir que d'effroyables précipices. J'appelle ainsi tant d'erreurs téméraires et extravagantes qu'on voyait paraître tous les jours. Ne croyez pas que ce soit seulement la querelle de l'épiscopat, ou quelques chicanes sur la liturgie anglicane, qui aient ému les Communes. Ces disputes n'étaient encore que de faibles commencements, par où ces esprits turbulents faisaient comme un essai de leur liberté. Mais quelque chose de plus violent se remuait dans le fond des cœurs : c'était un dégoût secret de tout ce qui a de l'autorité, et une démangeaison d'innover sans fin, après qu'on en a vu le premier exemple.

Ainsi les Calvinistes, plus hardis que les Luthériens, ont servi à établir les Sociniens, qui ont été plus loin qu'eux, et dont ils grossissent tous les jours le parti. Les sectes infinies des Anabaptistes sont sorties de cette même source ; et leurs opinions, mêlées au calvinisme, ont fait naître les Indépendants, qui n'ont point eu de bornes, parmi lesquels on voit les Trembleurs\footnote{En anglais, \textit{Quakers.}}, gens fanatiques, qui croient que toutes leurs rêveries leur sont inspirées ; et ceux qu'on nomme Chercheurs\footnote{\textit{Seekers.}}, à cause que dix-sept cents ans après \textsc{jésus-christ} ils cherchent encore la religion, et n'en ont point d'arrêtée.

C'est, Messieurs, en cette sorte que les esprits une fois émus, tombant de ruines en ruines, se sont divisés en tant de sectes. [...]

\bigskip

Il ne faut point s'étonner s'ils perdirent le respect de la majesté et des lois, ni s'ils devinrent factieux, rebelles et opiniâtres. On énerve la religion quand on la change, et on lui ôte un certain poids, qui seul est capable de tenir les peuples. Ils ont dans le fond du cœur je ne sais quoi d'inquiet qui s'échappe, si on leur ôte ce frein nécessaire, et on ne leur laisse plus rien à ménager quand on leur permet de se rendre maîtres de leur religion. C'est de là que nous est né ce prétendu règne de \textsc{christ}, inconnu jusques alors au christianisme, qui devait anéantir toute la royauté, et égaler tous les hommes ; songe séditieux des Indépendants, et leur chimère impie et sacrilège : tant il est vrai que tout se tourne en révoltes et en pensées séditieuses quand l'autorité de la religion est anéantie ! Mais pourquoi chercher des preuves d'une vérité que le Saint-Esprit a prononcée par une sentence manifeste ? Dieu même menace les peuples qui altèrent la religion qu'il a établie de se retirer du milieu d'eux, et par là de les livrer aux guerres civiles. Écoutez comme il parle par la bouche du prophète Zacharie : \textit{Leur âme,} dit le Seigneur, \textit{a varié envers moi,} quand ils ont si souvent changé la religion, \textit{et je leur ai dit : Je ne serai plus votre pasteur,} c'est-à-dire  : je vous abandonnerai à vous-mêmes, et à votre cruelle destinée ; et voyez la suite : \textit{Que ce qui doit mourir aille à la mort ; que ce qui doit être retranché soit retranché.} Entendez-vous ces paroles ? \textit{Et que ceux qui demeureront se dévorent les uns les autres.} Ô prophétie trop réelle, et trop véritablement accomplie ! [...]

\bigskip

Que si vous me demandez comment tant de factions opposées, et tant de sectes incompatibles, qui se devaient apparemment détruire les unes les autres, ont pu si opiniâtrement conspirer ensemble contre le trône royal, vous l'allez apprendre.

Un homme s'est rencontré\footnote{Cromwell.} d'une profondeur d'esprit incroyable, hypocrite raffiné autant qu'habile politique, capable de tout entreprendre et de tout cacher, également actif et infatigable dans la paix et dans la guerre, qui ne laissait rien à la fortune de ce qu'il pouvait lui ôter par conseil et par prévoyance ; mais au reste si vigilant et si prêt à tout, qu'il n'a jamais manqué les occasions qu'elle lui a présentées ; enfin un de ces esprits remuants et audacieux qui semblent être nés pour changer le monde. Que le sort de tels esprits est hasardeux, et qu'il en paraît dans l'histoire à qui leur audace a été funeste ! Mais aussi que ne font-ils pas, quand il plaît à Dieu de s'en servir ? Il fut donné à celui-ci de tromper les peuples, et de prévaloir contre les rois. Car, comme il eut aperçu que dans ce mélange infini de sectes, qui n'avaient plus de règles certaines, le plaisir de dogmatiser sans être repris ni contraint par aucune autorité ecclésiastique ni séculière était le charme qui possédait les esprits, il sut si bien les concilier par là qu'il fit un corps redoutable de cet assemblage monstrueux. Quand une fois on a trouvé le moyen de prendre la multitude par l'appât de la liberté, elle suit en aveugle, pourvu qu'elle en entende seulement le nom. Ceux-ci, occupés du premier objet qui les avait transportés, allaient toujours, sans regarder qu'ils allaient à la servitude, et leur subtil conducteur qui, en combattant, en dogmatisant, en mêlant mille personnages divers, en faisant le docteur et le prophète, aussi bien que le soldat et le capitaine, vit qu'il avait tellement enchanté le monde qu'il était regardé de toute l'armée comme un chef envoyé de Dieu pour la protection de l'indépendance, commença à s'apercevoir qu'il pouvait encore les pousser plus loin. Je ne vous raconterai pas la suite trop fortunée de ses entreprises, ni ses fameuses victoires dont la vertu était indignée, ni cette longue tranquillité qui a étonné l'univers.

\begin{flushright}
\large\textsc{bossuet}, \textit{Oraison funèbre de Henriette de France}.

\bigskip\footnotesize Œuvres complètes de Bossuet, volume XII.
\end{flushright}
\end{document}
