%~ \documentclass[a4paper,twocolumn,french]{article}
\documentclass[a4paper,french,17pt]{extarticle}

\usepackage{fontspec}
\usepackage[french]{babel}
\usepackage{microtype}
%~ \usepackage{polyglossia}
%~ \setdefaultlanguage{french}

\usepackage[margin=24mm,bmargin=24mm]{geometry}

%~ \setmainfont
%~ [
  %~ Path       = libre-baskerville/,
  %~ Extension  = .otf,
  %~ Ligatures  = TeX,
  %~ BoldFont   = LibreBaskerville-Bold,
  %~ ItalicFont = LibreBaskerville-Italic
%~ ]{LibreBaskerville-Regular}

%~ https://www.fontsquirrel.com/fonts/libre-baskerville

%~ \setmainfont{LibreBaskerville}

\setmainfont{EBGaramond}
[
  Extension      = .otf ,
  UprightFont    = *-Regular,
  ItalicFont     = *-Italic,
  BoldFont       = *-Bold,
  BoldItalicFont = *-BoldItalic,
  Numbers        = Lowercase,
  Ligatures      = Discretionary,
  Style          = Swash
]
% https://texnique.fr/osqa/questions/8423/lualatex-et-ligatures

\setlength{\columnsep}{8mm} 

\usepackage{lastpage}
\usepackage{fancyhdr}
\pagestyle{fancy}
\fancyhf{}
\fancyfoot[C]{\thepage \hspace{1pt} / \pageref{LastPage}}
\renewcommand{\headrulewidth}[0]{0pt}

\usepackage{perpage}
\MakePerPage{footnote}

\begin{document}

\thispagestyle{fancy}
\begin{center}
\textsc{sermon sur la mort}\\
\textsc{pour le mercredi}\\
\textsc{de la quatrième semaine de carême}\\
\textsc{prêché au louvre le 22 mars 1662}\\
\end{center}

\begin{flushright}
\small\textit{Domine, veni et vide.}\\
\small Seigneur, venez et voyez.\\
\small\textit{Joan.,} XI, 34.
\end{flushright}

Me sera-t-il permis aujourd'hui d'ouvrir un tombeau devant la cour, et des yeux si délicats ne seront-ils pas offensés par un objet si funèbre ? Je ne pense pas, Messieurs, que des chrétiens doivent refuser d'assister à ce spectacle avec Jésus-Christ. C'est à lui que l'on dit dans notre évangile : \frquote{Seigneur, venez et voyez} où l'on a déposé le corps de Lazare ; c'est lui qui ordonne qu'on lève la pierre, et qui semble nous dire à son tour : Venez et voyez vous-mêmes. Jésus ne refuse pas de voir ce corps mort comme un objet de pitié et un sujet de miracle ; mais c'est à nous, mortels misérables, de voir ce triste spectacle comme la conviction de nos erreurs. Allons et voyons avec Jésus-Christ, et désabusons-nous éternellement de tous les biens que la mort enlève.

C'est une étrange faiblesse de l'esprit humain, que jamais la mort ne lui soit présente, quoiqu'elle se mette en vue de tous côtés et en mille formes diverses. On n'entend dans les funérailles que des paroles d'étonnement de ce que ce mortel est mort : chacun rappelle en son souvenir depuis quel temps il lui a parlé et de quoi le défunt l'a entretenu ; et tout d'un coup il est mort. Voilà, dit-on, ce que c'est que l'homme ! Et celui qui le dit, c'est un homme ; et cet homme ne s'applique rien, oublieux de sa destinée ; ou, s'il passe dans son esprit quelque désir volage de s'y préparer, il dissipe bientôt ces noires idées ; et je puis dire dire, Messieurs, que les mortels n'ont pas moins de soin d'ensevelir les pensées de la mort que d'enterrer les morts mêmes. Mais peut-être que ces pensées feront plus d'effet dans nos cœurs, si
nous les méditons avec Jésus-Christ sur le tombeau du Lazare ; mais
demandons-lui qu'il nous les imprime par la grâce de son Saint-Esprit,
et tâchons de la mériter par l'entremise de la sainte Vierge.

Entre toutes les passions de l'esprit humain, l'une des plus violentes, c'est le désir de savoir ; et cette curiosité fait qu'il épuise ses forces pour trouver ou quelque secret inouï dans l'ordre de la nature, ou quelque adresse inconnue dans les ouvrages de l'art, ou quelque raffinement inusité dans la conduite des affaires. Mais parmi ces vastes désirs d'enrichir notre entendement par des connaissances nouvelles, la même chose nous arrive qu'à ceux qui, jetant bien loin leurs regards, ne remarquent pas les objets qui les environnent ; je veux dire que notre esprit s'étendant par de grands efforts sur des choses fort éloignées, et parcourant pour ainsi dire le ciel et la terre, passe cependant si légèrement sur ce qui se présente à lui de plus près, que nous consumons toute notre vie toujours ignorants de ce qui nous touche ; et non seulement de ce qui nous touche, mais encore de ce que nous sommes.

Il n'est rien de plus nécessaire que de recueillir en nous-mêmes toutes ces pensées qui s'égarent ; et c'est pour cela, chrétiens, que je vous invite aujourd'hui d'accompagner le Sauveur jusques au tombeau du Lazare : \textit{Veni et vide} : \frquote{Venez et voyez.} Ô mortels, venez contempler le spectacle des choses mortelles ; ô homme, venez apprendre ce que c'est que l'homme. Vous serez peut-être étonnés que je vous adresse à la mort pour être instruits de ce que vous êtes, et vous croirez que ce n'est pas bien représenter l'homme que de le montrer où il n'est plus. Mais si vous prenez soin de vouloir entendre ce qui se présente à nous dans le tombeau, vous accorderez aisément qu'il n'est point de plus véritable interprète ni de plus fidèle miroir des choses humaines.

La nature d'un composé ne se remarque jamais plus distinctement que dans la dissolution de ses parties. Comme elles s'altèrent mutuellement par le mélange, il faut les séparer pour les bien connaître. En effet la société de l'âme et du corps fait que le corps nous paraît quelque chose de plus qu'il n'est, et l'âme quelque chose de moins ; mais, lorsque venant à se séparer, le corps retourne à la terre et que l'âme aussi est mise en état de retourner au ciel d'où elle est tirée, nous voyons l'un et l'autre dans sa pureté. Ainsi nous n'avons qu'à considérer ce que la mort nous ravit et ce qu'elle laisse en son entier ; quelle partie de notre être tombe sous ses coups, et quelle autre se conserve dans cette ruine ; alors nous aurons compris ce que c'est que l'homme ; de sorte que je ne crains point d'assurer que c'est du sein de la mort et de ses ombres épaisses que sort une lumière immortelle pour éclairer nos esprits touchant l'état de notre nature. Accourez donc, ô mortels, et voyez dans le tombeau du Lazare ce que c'est que l'humanité : venez voir dans un même objet la fin de vos desseins et le commencement de vos espérances ; venez voir tout ensemble la dissolution et le renouvellement de votre être ; venez voir le triomphe de la vie dans la victoire de la mort : \textit{Veni et vide.}

Ô mort, nous te rendons grâces des lumières que tu répands sur notre ignorance. Toi seule nous convaincs de notre bassesse, toi seule nous fais connaître notre dignité. Si l'homme s'estime trop, tu sais déprimer son orgueil ; si l'homme se méprise trop, tu sais relever son courage ; et, pour réduire toutes ses pensées à un juste tempérament, tu lui apprends ces deux vérités qui lui ouvrent les yeux pour se bien connaître, qu'il est infiniment méprisable en tant qu'il passe, et infiniment estimable en tant qu'il aboutit à l'éternité. Ces deux importantes considérations feront le sujet de ce discours.

\begin{center}
\textsc{premier point}
\end{center}

C'est une entreprise hardie que d'aller dire aux hommes qu'ils sont peu de chose. Chacun est jaloux de ce qu'il est, et on aime mieux être aveugle que de connaître son faible. Surtout les grandes fortunes veulent être traitées délicatement ; elles ne prennent pas plaisir qu'on remarque leur défaut ; elles veulent que, si on le voit, du moins on le cache. Et toutefois, grâces à la mort, nous en pouvons parler avec liberté. Il n'est rien de si grand dans le monde qui ne reconnaisse en soi-même beaucoup de bassesse, à le considérer par cet endroit-là. Mais c'est encore trop de vanité de distinguer en nous la partie faible, comme si nous avions quelque chose de considérable. Vive l'Éternel ! ô grandeur humaine, de quelque côté que je t'envisage (sinon en tant que tu viens de Dieu et que tu dois être rapportée à Dieu, car en cette sorte je découvre en toi un rayon de la Divinité qui attire justement mes respects) ; mais en tant que tu es purement humaine, je le dis encore une fois, de quelque côté que je t'envisage, je ne vois rien en toi que je considère, parce que, de quelque endroit que je te tourne, je trouve toujours la mort en face, qui répand tant d'ombres de toutes parts sur ce que l'éclat du monde voulait colorer, que je ne sais plus sur quoi appuyer ce nom auguste de grandeur, ni à quoi je puis appliquer un si beau titre.

Convainquons-nous, chrétiens, de cette importante vérité par un raisonnement invincible. L'accident ne peut pas être plus noble que la substance, ni l'accessoire plus considérable que le principal, ni le bâtiment plus solide que le fonds sur lequel il est élevé, ni enfin ce qui est attaché à notre être plus grand ni plus important que notre être même. Maintenant, qu'est-ce que notre être ? Dites-le nous, ô mort ; car les hommes superbes ne m'en croiraient pas. Mais, ô mort, vous êtes muette et vous ne parlez qu'aux yeux. Un grand roi va vous prêter sa voix, afin que vous vous fassiez entendre aux oreilles et que vous portiez dans les cœurs des vérités plus articulées.

Voici la belle méditation dont David s'entretenait sur le trône, et au milieu de sa cour : Sire, elle est digne de votre audience. \textit{Ecce mensurabiles posuisti dies meos, et substantia mea tanquam nihilum ante te}\footnote{\textit{Psal.} XXXVIII, 6.} : Ô éternel Roi des siècles, vous êtes toujours à vous-même, toujours en vous-même ; votre être éternellement immuable, ni se s'écoule, ni ne se change, ni ne se mesure. \frquote{Et voici que vous avez fait mes jours mesurables, et ma substance n'est rien devant vous.} Non, ma substance n'est rien devant vous ; et tout l'être qui se mesure n'est rien, parce que ce qui se mesure a son terme, et lorsqu'on est venu à ce terme, un dernier point détruit tout, comme si jamais il n'avait été. Qu'est-ce que cent ans, qu'est-ce que mille ans, puisqu'un seul moment les efface ? Multipliez vos jours, comme les cerfs que la fable ou l'histoire de la nature fait vivre durant tant de siècles ; durez autant que ces grands chênes sous lesquels nos ancêtres se sont reposés, et qui donneront encore de l'ombre à notre postérité ; entassez dans cet espace qui paraît immense, honneurs, richesses, plaisirs ; que vous profitera cet amas, puisque le dernier souffle de la mort, tout faible, tout languissant, abattra tout à coup cette vaine pompe avec la même facilité qu'un château de cartes, vain amusement des enfants ? Que vous servira d'avoir tant écrit dans ce livre, d'en avoir rempli toutes les pages de beaux caractères, puisque enfin une seule rature doit tout effacer ? Encore une rature laisserait-elle quelques traces du moins d'elle-même ; au lieu que ce dernier moment qui effacera d'un seul trait toute votre vie, s'ira perdre lui-même avec tout le reste dans ce grand gouffre du néant. Il n'y aura plus sur la terre aucun vestige de ce que nous sommes ; la chair changera de nature ; le corps prendra un autre nom ; \frquote{même celui de cadavre ne lui demeurera pas longtemps ;} il deviendra, dit Tertullien, un je ne sais quoi qui n'a plus de nom dans aucune langue : tant il est vrai que tout meurt en lui, jusqu'à ces termes funèbres par lesquels on exprimait ses malheureux restes ! \textit{Post totum ignobilitatis elogium, caducœ in originem terram, et cadaveris nomen ; et de isto quoque nomme periturœ in nullum inde jam nomen, in omnis jam vocabuli mortem}\footnote{\textit{De Resurrect. carn.,} n. 4.}.

Qu'est-ce donc que ma substance, ô grand Dieu ? J'entre dans la vie pour en sortir aussitôt ; je viens me montrer comme les autres ; après, il faudra disparaître. Tout nous appelle à la mort. La nature, presque envieuse du bien qu'elle nous a fait, nous déclare souvent et nous fait signifier qu'elle ne peut pas nous laisser longtemps ce peu de matière qu'elle nous prête, qui ne doit pas demeurer dans les mêmes mains, et qui doit être éternellement dans le commerce : elle en a besoin pour d'autres formes, elle la redemande pour d'autres ouvrages. Cette recrue continuelle du genre humain, je veux dire les enfants qui naissent, à mesure qu'ils croissent et qu'ils s'avancent, semblent nous pousser de l'épaule et nous dire : Retirez-vous, c'est maintenant notre tour. Ainsi, comme nous en voyons passer d'autres devant nous, d'autres nous verront passer, qui doivent à leurs successeurs le même spectacle. Ô Dieu ! encore une fois, qu'est-ce que de nous ? Si je jette la vue devant moi, quel espace infini où je ne suis pas ! Si je la retourne en arrière, quelle suite effroyable où je ne suis plus, et que j'occupe peu de place dans cet abîme immense du temps ! Je ne suis rien ; un si petit intervalle n'est pas capable de me distinguer du néant. On ne m'a envoyé que pour faire nombre, encore n'avait-on que faire de moi ; et la pièce n'en aurait pas été moins jouée, quand je serais demeuré derrière le théâtre.

Encore si nous voulons discuter les choses dans une considération plus subtile, ce n'est pas toute l'étendue de notre vie qui nous distingue du néant ; et vous savez, chrétiens, qu'il n'y jamais qu'un moment qui nous en sépare. Maintenant nous en tenons un ; maintenant il périt ; et avec lui nous péririons tous, si promptement et sans perdre temps nous n'en saisissions un autre semblable, jusqu'à ce qu'enfin il en viendra un auquel nous ne pourrons arriver, quelque effort que nous fassions pour nous y étendre ; et alors nous tomberons tout à coup, manque de soutien. Ô fragile appui de notre être ! Ô fondement ruineux de notre substance ! \textit{In imagine pertransit homo}\footnote{\textit{Psal.} XXXVIII, 7.} : ah ! l'homme passe vraiment de même qu'une ombre ou de même qu'une image en figure ; et comme lui-même n'est rien de solide, il ne poursuit aussi que des choses vaines, l'image du bien, et non le bien même. Que la place est petite que nous occupons en ce monde : si petite certainement et si peu considérable, que je doute quelquefois avec Arnobe si je dors ou si je veille : \textit{Vigilemus aliquando, an ipsum vigilare, quod dicitur somni sit perpetui portio}\footnote{\textit{Advers. Gent.,} lib. II.}. Je ne sais si ce que j'appelle veiller n'est peut-être pas une partie un peu plus animée d'un sommeil profond ; et si je vois des choses réelles, ou si je suis seulement troublé par des fantaisies et par de vains simulacres. \textit{Præterit figura hujus mundi}\footnote{\textit{I Cor.,} VII, 31.} : \frquote{La figure de ce monde passe, et ma substance n'est rien devant Dieu :} \textit{Et substantia mea tanquam nihilum ante te}\footnote{\textit{Psal.} XXXVIII, 6.}.

\begin{center}
\textsc{second point}
\end{center}

N'en doutons pas, chrétiens, quoique nous soyons relégués dans cette
dernière partie de l'univers qui est le théâtre des changements et
l'empire de la mort ; bien plus, quoiqu'elle nous soit inhérente et que
nous la portions dans notre sein, toutefois au milieu de cette matière
et à travers l'obscurité de nos connaissances qui vient des
préjugés de nos sens, si nous savons rentrer en nous-mêmes, nous y
trouverons quelque chose qui montre bien par une certaine vigueur
son origine céleste, et qui n'appréhende pas la corruption.

Je ne suis pas de ceux qui font grand état des connaissances humaines ;
et je confesse néanmoins que je ne puis contempler sans admiration ces
merveilleuses découvertes qu'a faites la science pour pénétrer la nature
, ni tant de belles inventions que l'art a trouvées pour l'accommoder à
notre usage. L'homme a presque changé la face du monde ; il a su dompter
par l'esprit les animaux qui le surmontaient par la force ; il a su
discipliner leur humeur brutale et contraindre leur liberté indocile ;
il a même fléchi par adresse les créatures inanimées. La terre
n'a-t-elle pas été forcée par son industrie à lui donner des aliments
plus convenables, les plantes à corriger en sa faveur leur aigreur
sauvage, les venins mêmes à se tourner en remèdes pour l'amour de
lui ? Il serait superflu de vous raconter comme il sait ménager les
éléments, après tant de sortes de miracles qu'il fait faire tous les
jours aux plus intraitables, je veux dire au feu et à l'eau, ces deux
grands ennemis, qui s'accordent néanmoins à nous servir dans des opérations si utiles et si nécessaires. Quoi plus? il est
monté jusqu'aux cieux ; pour marcher plus sûrement, il a appris aux
astres à le guider dans ses voyages ; pour mesurer plus également sa
vie, il a obligé le soleil à rendre compte pour ainsi dire de tous ses
pas. Mais laissons à la rhétorique cette longue et scrupuleuse
énumération ; et contentons-nous de remarquer en théologiens que Dieu
ayant formé l'homme, dit l'oracle de l'Écriture, pour être le chef de
l'univers, d'une si noble institution, quoique changée par son crime, il
lui a laissé un certain instinct de chercher ce qui lui manque dans
toute l'étendue de la nature. C'est pourquoi, si je l'ose dire, il
fouille partout hardiment comme dans son bien, et il n'y a aucune partie
de l'univers où il n'ait signalé son industrie.

Pensez maintenant, Messieurs, comment aurait pu prendre un tel ascendant
une créature si faible et si exposée selon le corps aux insultes de
toutes les autres, si elle n'avait en son esprit une force
supérieure à toute la nature visible, un souffle immortel de l'Esprit de
Dieu, un rayon de sa face, un trait de sa ressemblance. Non, non, il ne
se peut autrement. Si un excellent ouvrier a fait quelque rare machine,
aucun ne peut s'en servir que par les lumières qu'il donne. Dieu a
fabriqué le monde comme une grande machine que sa seule sagesse pouvait
inventer, que sa seule puissance pouvait construire. Ô homme, il
t'a établi pour t'en servir ; il a mis pour ainsi dire en tes mains toute
la nature, pour l'appliquer à tes usages ; il t'a même permis de l'orner
et de l'embellir par ton art. Car qu'est-ce autre chose que l'art, sinon
l'embellissement de la nature ? Tu peux ajouter quelques couleurs pour
orner cet admirable tableau ; mais comment pourrais-tu faire remuer tant
soit peu une machine si forte et si délicate ; ou de quelle sorte
pourrais-tu faire seulement un trait convenable dans une peinture si
riche, s'il n'y avait en toi-même et dans quelque partie de ton être
quelque art dérivé de ce premier art, quelques fécondes idées tirées de
ces idées originales, en un mot quelque ressemblance, quelque écoulement, quelque portion de cet esprit
ouvrier qui a fait le monde ? Que s'il est ainsi, chrétiens, qui ne
voit que toute la nature conjurée ensemble n'est pas capable d'éteindre
un si beau rayon, cette partie de nous-mêmes qui porte un
caractère si noble de la puissance divine qui la soutient ; et qu'ainsi
notre âme supérieure au monde et à toutes les vertus qui le composent,
n'a rien à craindre que de son auteur?

Mais continuons, chrétiens, une méditation si utile de l'image de Dieu
en nous ; et voyons de quelle manière cette créature chérie, destinée à
se servir de toutes les autres, se prescrit à elle-même ce qu'elle doit
faire. Dans la corruption où nous sommes, je confesse que c'est ici
notre faible ; et toutefois je ne puis considérer sans admiration
ces règles immuables des mœurs que la raison a posées. Quoi ! cette âme
plongée dans le corps, qui en épouse toutes les passions avec tant
d'attache, qui languit, qui se désespère, qui n'est plus à elle-même
quand il souffre, dans quelle lumière a-t-elle vu qu'elle eût
néanmoins sa félicité à part ? qu'elle dût dire quelquefois hardiment,
tous les sens, toutes les passions, et presque toute la nature criant à
l'encontre : \textit{Ce m'est un gain de mourir}\footnote{\textit{Philip.,} I, 21.} ; — \textit{Je me réjouis dans
les afflictions}\footnote{\textit{Coloss.,} I, 24.} ? Ne faut-il pas, chrétiens, qu'elle ait découvert
intérieurement une beauté bien exquise dans ce qui s'appelle devoir,
pour oser assurer positivement qu'elle doit s'exposer sans crainte,
qu'il faut s'exposer même avec joie à des fatigues immenses, à des
douleurs incroyables et à une mort assurée pour les amis, pour la
patrie, pour le prince, pour les autels? Et n'est-ce pas une espèce de
miracle que ces maximes constantes de courage, de probité, de justice,
ne pouvant jamais être abolies, je ne dis pas par le temps, mais par un
usage contraire, il y ait pour le bonheur du genre humain beaucoup moins
de personnes qui les décrient tout à fait qu'il n'y en a qui les
pratiquent parfaitement.

Sans doute il y a au dedans de nous une divine clarté : \frquote{Un rayon de
votre face, ô Seigneur, s'est imprimé en nos âmes :}
\textit{Signatum est super nos lumen vultùs tui, Domine}\footnote{\textit{Psal.} IV, 7.}. C'est là que nous
découvrons, comme dans un globe de lumière, un agrément immortel dans
l'honnêteté et la vertu ; c'est la première raison qui se montre à
nous par son image ; c'est la vérité elle-même qui nous parle et
qui doit bien nous faire entendre qu'il y a quelque chose en nous qui ne
meurt pas, puisque Dieu nous a faits capables de trouver du bonheur même
dans la mort.

Tout cela n'est rien, chrétiens ; et voici le trait le plus admirable de
cette divine ressemblance. Dieu se connaît et se contemple ; sa vie
c'est de se connaître ; et parce que l'homme est son image, il veut
aussi qu'il le connaisse. Être éternel, immense, infini, exempt de
toute matière, libre de toutes limites, dégagé de toute imperfection,
chrétiens, quel est ce miracle ? Nous qui ne sentons rien que de borné,
qui ne voyons rien que de muable, où avons-nous pu comprendre cette
éternité ? où avons-nous songé cette infinité? Ô éternité ! ô infinité!
dit saint Augustin, que nos sens ne soupçonnent seulement pas, par où
donc es-tu entrée dans nos âmes? Mais si nous sommes tout corps et tout
matière, comment pouvons-nous concevoir un esprit pur, et comment
avons-nous pu seulement inventer ce nom ?

Je sais ce que l'on peut dire en ce lieu, et avec raison que lorsque
nous parlons de ces esprits, nous n'entendons pas trop ce que nous
disons ; notre faible imagination ne pouvant soutenir une idée si pure,
lui présente toujours quelque petit corps pour la revêtir. Mais après
qu'elle a fait son dernier effort pour les rendre bien subtils et bien
déliés, ne sentez-vous pas en même temps qu'il sort du fond de notre âme
une lumière céleste qui dissipe tous ces fantômes, si minces et si
délicats que nous ayons pu les figurer ? Si vous la pressez davantage et
que vous lui demandiez ce que c'est, une voix s'élèvera du centre de
l'âme : Je ne sais pas ce que c'est, mais néanmoins ce n'est pas
cela. Quelle force , quelle énergie, quelle secrète vertu sent en
elle-même cette âme pour se corriger, se démentir elle-même et pour oser
rejeter tout ce qu'elle pense ! Qui ne voit qu'il y a en elle un ressort caché qui
n'agit pas encore de toute sa force, et lequel, quoiqu'il soit
contraint, quoiqu'il n'ait pas son mouvement libre, fait bien voir par
une certaine vigueur qu'il ne tient pas tout entier à la matière et
qu'il est comme attaché par sa pointe à quelque principe plus haut 
?

Il est vrai, chrétiens, je le confesse, nous ne soutenons pas longtemps
cette noble ardeur ; ces belles idées s'épaississent bientôt, et l'âme
se replonge bientôt dans sa matière. Elle a ses faiblesses, elle a ses
langueurs ; et permettez-moi de le dire, car je ne sais plus comment
m'exprimer, elle a des grossièretés incompréhensibles qui, si elle n'est
éclairée d'ailleurs, la forcent presque elle-même de douter de ce
qu'elle est. C'est pourquoi les sages du monde voyant l'homme d'un
côté si grand, de l'autre si méprisable , n'ont su ni que penser ni que
dire d'une si étrange composition. Demandez aux philosophes profanes ce
que c'est que l'homme ; les uns en feront un dieu, les autres en feront
un rien ; les uns diront que la nature le chérit comme une mère et
qu'elle en fait ses délices ; les autres, qu'elle l'expose comme une
marâtre et qu'elle en fait son rebut ; et un troisième parti ne sachant
plus que deviner touchant la cause de ce grand mélange, répondra qu'elle
s'est jouée en unissant deux pièces qui n'ont nul rapport, et ainsi que
par une espèce de caprice elle a formé ce prodige qu'on appelle l'homme.

Vous jugez bien, Messieurs, que ni les uns ni les autres n'ont donné au
but, et qu'il n'y a plus que la foi qui puisse expliquer une si grande
énigme. Vous vous trompez, ô sages du siècle : l'homme n'est pas les
délices de la nature, puisqu'elle l'outrage en tant de manières ;
l'homme ne peut non plus être son rebut, puisqu'il a quelque chose en
lui qui vaut mieux que la nature elle-même, je parle de la nature
sensible. D'où vient donc une si étrange disproportion? Faut-il,
chrétiens, que je vous le dise, et ces masures mal assorties, avec ces
fondements si magnifiques, ne crient-elles pas assez haut que
l'ouvrage n'est pas en son entier ? Contemplez cet édifice, vous y verrez des marques d'une main
divine ; mais l'inégalité de l'ouvrage vous fera bientôt remarquer ce
que le péché a mêlé du sien. Ô Dieu ! quel est ce mélange? J'ai peine à
me reconnaître ; peu s'en faut que je ne m'écrie avec le prophète :
\textit{Hœccine est urbs perfecti decoris, gaudium universœ terrœ ?}\footnote{\textit{Thren.,} II, 15.} Est-ce
là cette Jérusalem ? \frquote{Est-ce là cette ville ? est-ce là ce temple,
l'honneur et la joie de toute la terre ?} Et moi je dis : Est-ce là cet
homme fait à l'image de Dieu, le miracle de sa sagesse et le
chef-d'œuvre de ses mains ?

C'est lui-même, n'en doutez pas. D'où vient donc cette discordance, et
pourquoi vois-je ces parties si mal rapportées ? C'est que l'homme a
voulu bâtir à sa mode sur l'ouvrage de son Créateur, et il s'est éloigné
du plan : ainsi contre la régularité du premier dessein, l'immortel et
le corruptible, le spirituel et le charnel , l'ange et la bête en un
mot, se sont trouvés tout à coup unis. Voilà le mot de l'énigme, voilà
le dégagement de tout l'embarras : la foi nous a rendus à nous-mêmes, et
nos faiblesses honteuses ne peuvent plus nous cacher notre dignité
naturelle.

Mais, hélas! que nous profite cette dignité ? Quoique nos ruines
respirent encore quelque air de grandeur, nous n'en sommes pas moins
accablés dessous ; notre ancienne immortalité ne sert qu'à nous rendre
plus insupportable la tyrannie de la mort ; et quoique nos âmes lui
échappent, si cependant le péché les rend misérables, elles n'ont pas de
quoi se vanter d'une éternité si onéreuse. Que dirons-nous, chrétiens?
que répondrons-nous à une plainte si pressante? Jésus-Christ y répondra
dans notre évangile. Il vient voir le Lazare décédé, il vient visiter la
nature humaine qui gémit sous l'empire de la mort. Ah ! cette visite
n'est pas sans cause. C'est l'ouvrier même qui vient en personne pour
reconnaître ce qui manque à son édifice. C'est qu'il a dessein de le
reformer suivant son premier modèle : \textit{secundùm imaginera ejus qui
creavit illum}\footnote{\textit{Coloss.,} III, 10.}.

Ô âme remplie de crimes, tu crains avec raison l'immortalité qui
rendrait ta mort éternelle. Mais voici en la personne de Jésus-Christ la résurrection et la vie\footnote{\textit{Joan.,} XI, 25, 26.} : qui croit en lui ne meurt pas ;
qui croit en lui est déjà vivant d'une vie spirituelle et intérieure,
vivant par la vie de la grâce qui attire après elle la vie de la gloire.
Mais le corps est cependant sujet à la mort. Ô âme, console-toi.
Si ce divin Architecte qui a entrepris de te réparer, laisse tomber
pièce à pièce ce vieux bâtiment de ton corps, c'est qu'il veut te le
rendre en meilleur état, c'est qu'il veut le rebâtir dans un meilleur
ordre ; il entrera pour un peu de temps dans l'empire de la mort, mais
il ne laissera rien entre ses mains, si ce n'est la mortalité.

Ne vous persuadez pas que nous devions regarder la corruption selon les
raisonnements de la médecine, comme une suite naturelle de la
composition et du mélange. Il faut élever plus haut nos esprits, et
croire selon les principes du christianisme que ce qui engage la chair à
la nécessité d'être corrompue, c'est qu'elle est un attrait au mal, une
source de mauvais désirs, enfin une \frquote{chair de péché\footnote{\textit{Rom.,} VIII, 3.},} comme parle
le saint Apôtre. Une telle chair doit être détruite, je dis même dans
les élus, parce qu'en cet état de chair de péché, elle ne mérite pas
d'être réunie à une âme bienheureuse, ni d'entrer dans le royaume de
Dieu : \textit{Caro et sanguis regnum Dei possidere non possunt}\footnote{\textit{I Cor.,} XV, 50.}. Il faut
donc qu'elle change sa première forme afin d'être renouvelée, et qu'elle
perde tout son premier être pour en recevoir un second de la main de
Dieu. Comme un vieux bâtiment irrégulier qu'on néglige de réparer,
afin de le dresser de nouveau dans un plus bel ordre d'architecture ;
ainsi cette chair toute déréglée par le péché et la convoitise, Dieu la
laisse tomber en ruine, afin de la refaire à sa mode et selon le premier
plan de sa création. Elle doit être réduite en poudre, parce qu'elle a
servi au péché.

Ne vois-tu pas le divin Jésus qui fait ouvrir le tombeau ? C'est le
prince qui fait ouvrir la prison aux misérables captifs. Les corps morts
qui sont enfermés dedans entendront un jour sa parole, et ils
ressusciteront comme le Lazare ; ils ressusciteront mieux que le Lazare, parce qu'ils ressusciteront pour ne mourir plus, et que la
mort, dit le Saint-Esprit, sera noyée dans l'abime pour ne
paraître jamais : \textit{Et mors ultra non erit ampliùs}\footnote{\textit{Apoc.,} XXI, 4.}.

Que crains-tu donc, âme chrétienne, dans les approches de la mort ?
Peut-être qu'en voyant tomber ta maison tu appréhendes d'être sans
retraite? Mais écoute le divin Apôtre : \frquote{Nous savons,} nous savons,
dit-il, nous ne sommes pas induits à le croire par des conjectures
douteuses, mais nous le savons très-assurément et avec une entière
certitude, \frquote{que si cette maison de terre et de boue dans laquelle nous
habitons est détruite, nous avons une autre maison qui nous est préparée
au ciel\footnote{\textit{II Cor.,} V, 1.}.} Ô conduite miséricordieuse de celui qui pourvoit à nos
besoins ! Il a dessein, dit excellemment saint Jean Chrysostome\footnote{\textit{Homil. In Dict. Apost.,}
De dormientibus, etc.}, de réparer la maison qu'il nous a donnée ; pendant qu'il la détruit
et qu'il la renverse pour la refaire toute neuve, il est nécessaire que
nous délogions ; car que ferions-nous dans cette poudre, dans ce
tumulte, dans cet embarras ? Et lui-même nous offre son palais ; il nous
donne un appartement pour nous faire attendre en repos l'entière
réparation de notre ancien édifice.

\begin{flushright}
\large
\textsc{bossuet}, \textit{Sermon sur la mort}.
\end{flushright}
\end{document}
