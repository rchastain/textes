\documentclass[a4paper,twocolumn,french]{article}

\usepackage{fontspec}
\usepackage[french]{babel}
\usepackage{microtype}
%~ \usepackage{polyglossia}
%~ \setdefaultlanguage{french}

\usepackage[margin=16mm,bmargin=24mm]{geometry}

%~ \setmainfont
%~ [
  %~ Path       = libre-baskerville/,
  %~ Extension  = .otf,
  %~ Ligatures  = TeX,
  %~ BoldFont   = LibreBaskerville-Bold,
  %~ ItalicFont = LibreBaskerville-Italic
%~ ]{LibreBaskerville-Regular}

%~ https://www.fontsquirrel.com/fonts/libre-baskerville

%~ \setmainfont{LibreBaskerville}

\setmainfont{EBGaramond}
[
  Extension      = .otf ,
  UprightFont    = *-Regular,
  ItalicFont     = *-Italic,
  BoldFont       = *-Bold,
  BoldItalicFont = *-BoldItalic,
  Numbers        = Lowercase,
  Ligatures      = Discretionary,
  Style          = Swash
]
% https://texnique.fr/osqa/questions/8423/lualatex-et-ligatures

\setlength{\columnsep}{8mm} 

\usepackage{lastpage}
\usepackage{fancyhdr}
\pagestyle{fancy}
\fancyhf{}
\fancyfoot[C]{\thepage \hspace{1pt} / \pageref{LastPage}}
\renewcommand{\headrulewidth}[0]{0pt}

\begin{document}

\thispagestyle{fancy}
\begin{center}
\textsc{sermon sur la mort}\\
\textsc{pour le mercredi}\\
\textsc{de la quatrième semaine de carême}\\
\textsc{prêché au louvre le 22 mars 1662}\\
\end{center}

\begin{flushright}
\small\textit{Domine, veni et vide.}\\
\small Seigneur, venez et voyez.\\
\small\textit{Joan.,} XI, 34.
\end{flushright}

Me sera-t-il permis aujourd'hui d'ouvrir un tombeau devant la cour, et des yeux si délicats ne seront-ils pas offensés par un objet si funèbre ? Je ne pense pas, Messieurs, que des chrétiens doivent refuser d'assister à ce spectacle avec Jésus-Christ. C'est à lui que l'on dit dans notre évangile : \frquote{Seigneur, venez et voyez} où l'on a déposé le corps de Lazare ; c'est lui qui ordonne qu'on lève la pierre, et qui semble nous dire à son tour : Venez et voyez vous-mêmes. Jésus ne refuse pas de voir ce corps mort comme un objet de pitié et un sujet de miracle ; mais c'est à nous, mortels misérables, de voir ce triste spectacle comme la conviction de nos erreurs. Allons et voyons avec Jésus-Christ, et désabusons-nous éternellement de tous les biens que la mort enlève.

C'est une étrange faiblesse de l'esprit humain, que jamais la mort ne lui soit présente, quoiqu'elle se mette en vue de tous côtés et en mille formes diverses. On n'entend dans les funérailles que des paroles d'étonnement de ce que ce mortel est mort : chacun rappelle en son souvenir depuis quel temps il lui a parlé et de quoi le défunt l'a entretenu ; et tout d'un coup il est mort. Voilà, dit-on, ce que c'est que l'homme ! Et celui qui le dit, c'est un homme ; et cet homme ne s'applique rien, oublieux de sa destinée ; ou, s'il passe dans son esprit quelque désir volage de s'y préparer, il dissipe bientôt ces noires idées ; et je puis dire dire, Messieurs, que les mortels n'ont pas moins de soin d'ensevelir les pensées de la mort que d'enterrer les morts mêmes. [...]

Entre toutes les passions de l'esprit humain, l'une des plus violentes, c'est le désir de savoir ; et cette curiosité fait qu'il épuise ses forces pour trouver ou quelque secret inouï dans l'ordre de la nature, ou quelque adresse inconnue dans les ouvrages de l'art, ou quelque raffinement inusité dans la conduite des affaires. Mais parmi ces vastes désirs d'enrichir notre entendement par des connaissances nouvelles, la même chose nous arrive qu'à ceux qui, jetant bien loin leurs regards, ne remarquent pas les objets qui les environnent ; je veux dire que notre esprit s'étendant par de grands efforts sur des choses fort éloignées, et parcourant pour ainsi dire le ciel et la terre, passe cependant si légèrement sur ce qui se présente à lui de plus près, que nous consumons toute notre vie toujours ignorants de ce qui nous touche ; et non seulement de ce qui nous touche, mais encore de ce que nous sommes.

Il n'est rien de plus nécessaire que de recueillir en nous-mêmes toutes ces pensées qui s'égarent ; et c'est pour cela, chrétiens, que je vous invite aujourd'hui d'accompagner le Sauveur jusques au tombeau du Lazare : \textit{Veni et vide} : \frquote{Venez et voyez.} Ô mortels, venez contempler le spectacle des choses mortelles ; ô homme, venez apprendre ce que c'est que l'homme. Vous serez peut-être étonnés que je vous adresse à la mort pour être instruits de ce que vous êtes, et vous croirez que ce n'est pas bien représenter l'homme que de le montrer où il n'est plus. Mais si vous prenez soin de vouloir entendre ce qui se présente à nous dans le tombeau, vous accorderez aisément qu'il n'est point de plus véritable interprète ni de plus fidèle miroir des choses humaines.

La nature d'un composé ne se remarque jamais plus distinctement que dans la dissolution de ses parties. Comme elles s'altèrent mutuellement par le mélange, il faut les séparer pour les bien connaître. En effet la société de l'âme et du corps fait que le corps nous paraît quelque chose de plus qu'il n'est, et l'âme quelque chose de moins ; mais, lorsque venant à se séparer, le corps retourne à la terre et que l'âme aussi est mise en état de retourner au ciel d'où elle est tirée, nous voyons l'un et l'autre dans sa pureté. Ainsi nous n'avons qu'à considérer ce que la mort nous ravit et ce qu'elle laisse en son entier ; quelle partie de notre être tombe sous ses coups, et quelle autre se conserve dans cette ruine ; alors nous aurons compris ce que c'est que l'homme ; de sorte que je ne crains point d'assurer que c'est du sein de la mort et de ses ombres épaisses que sort une lumière immortelle pour éclairer nos esprits touchant l'état de notre nature. Accourez donc, ô mortels, et voyez dans le tombeau du Lazare ce que c'est que l'humanité : venez voir dans un même objet la fin de vos desseins et le commencement de vos espérances ; venez voir tout ensemble la dissolution et le renouvellement de votre être ; venez voir le triomphe de la vie dans la victoire de la mort : \textit{Veni et vide.}

Ô mort, nous te rendons grâces des lumières que tu répands sur notre ignorance. Toi seule nous convaincs de notre bassesse, toi seule nous fais connaître notre dignité. Si l'homme s'estime trop, tu sais déprimer son orgueil ; si l'homme se méprise trop, tu sais relever son courage ; et, pour réduire toutes ses pensées à un juste tempérament, tu lui apprends ces deux vérités qui lui ouvrent les yeux pour se bien connaître, qu'il est infiniment méprisable en tant qu'il passe, et infiniment estimable en tant qu'il aboutit à l'éternité. Ces deux importantes considérations feront le sujet de ce discours.

\begin{center}
\textsc{premier point}
\end{center}

C'est une entreprise hardie que d'aller dire aux hommes qu'ils sont peu de chose. Chacun est jaloux de ce qu'il est, et on aime mieux être aveugle que de connaître son faible. Surtout les grandes fortunes veulent être traitées délicatement ; elles ne prennent pas plaisir qu'on remarque leur défaut ; elles veulent que, si on le voit, du moins on le cache. Et toutefois, grâces à la mort, nous en pouvons parler avec liberté. Il n'est rien de si grand dans le monde qui ne reconnaisse en soi-même beaucoup de bassesse, à le considérer par cet endroit-là. Mais c'est encore trop de vanité de distinguer en nous la partie faible, comme si nous avions quelque chose de considérable. Vive l'Éternel ! ô grandeur humaine, de quelque côté que je t'envisage (sinon en tant que tu viens de Dieu et que tu dois être rapportée à Dieu, car en cette sorte je découvre en toi un rayon de la Divinité qui attire justement mes respects) ; mais en tant que tu es purement humaine, je le dis encore une fois, de quelque côté que je t'envisage, je ne vois rien en toi que je considère, parce que, de quelque endroit que je te tourne, je trouve toujours la mort en face, qui répand tant d'ombres de toutes parts sur ce que l'éclat du monde voulait colorer, que je ne sais plus sur quoi appuyer ce nom auguste de grandeur, ni à quoi je puis appliquer un si beau titre.

Convainquons-nous, chrétiens, de cette importante vérité par un raisonnement invincible. L'accident ne peut pas être plus noble que la substance, ni l'accessoire plus considérable que le principal, ni le bâtiment plus solide que le fonds sur lequel il est élevé, ni enfin ce qui est attaché à notre être plus grand ni plus important que notre être même. Maintenant, qu'est-ce que notre être ? Dites-le nous, ô mort ; car les hommes superbes ne m'en croiraient pas. Mais, ô mort, vous êtes muette et vous ne parlez qu'aux yeux. Un grand roi va vous prêter sa voix, afin que vous vous fassiez entendre aux oreilles et que vous portiez dans les cœurs des vérités plus articulées.

Voici la belle méditation dont David s'entretenait sur le trône, et au milieu de sa cour : Sire, elle est digne de votre audience. \textit{Ecce mensurabiles posuisti dies meos, et substantia mea tanquam nihilum ante te} : Ô éternel Roi des siècles, vous êtes toujours à vous-même, toujours en vous-même ; votre être éternellement immuable, ni se s'écoule, ni ne se change, ni ne se mesure. \frquote{Et voici que vous avez fait mes jours mesurables, et ma substance n'est rien devant vous.} Non, ma substance n'est rien devant vous ; et tout l'être qui se mesure n'est rien, parce que ce qui se mesure a son terme, et lorsqu'on est venu à ce terme, un dernier point détruit tout, comme si jamais il n'avait été. Qu'est-ce que cent ans, qu'est-ce que mille ans, puisqu'un seul moment les efface ? Multipliez vos jours, comme les cerfs que la fable ou l'histoire de la nature fait vivre durant tant de siècles ; durez autant que ces grands chênes sous lesquels nos ancêtres se sont reposés, et qui donneront encore de l'ombre à notre postérité ; entassez dans cet espace qui paraît immense, honneurs, richesses, plaisirs ; que vous profitera cet amas, puisque le dernier souffle de la mort, tout faible, tout languissant, abattra tout à coup cette vaine pompe avec la même facilité qu'un château de cartes, vain amusement des enfants ? Que vous servira d'avoir tant écrit dans ce livre, d'en avoir rempli toutes les pages de beaux caractères, puisque enfin une seule rature doit tout effacer ? Encore une rature laisserait-elle quelques traces du moins d'elle-même ; au lieu que ce dernier moment qui effacera d'un seul trait toute votre vie, s'ira perdre lui-même avec tout le reste dans ce grand gouffre du néant. Il n'y aura plus sur la terre aucun vestige de ce que nous sommes ; la chair changera de nature ; le corps prendra un autre nom ; \frquote{même celui de cadavre ne lui demeurera pas longtemps ;} il deviendra, dit Tertullien, un je ne sais quoi qui n'a plus de nom dans aucune langue : tant il est vrai que tout meurt en lui, jusqu'à ces termes funèbres par lesquels on exprimait ses malheureux restes !

Qu'est-ce donc que ma substance, ô grand Dieu ? J'entre dans la vie pour en sortir aussitôt ; je viens me montrer comme les autres ; après, il faudra disparaître. Tout nous appelle à la mort. La nature, presque envieuse du bien qu'elle nous a fait, nous déclare souvent et nous fait signifier qu'elle ne peut pas nous laisser longtemps ce peu de matière qu'elle nous prête, qui ne doit pas demeurer dans les mêmes mains, et qui doit être éternellement dans le commerce : elle en a besoin pour d'autres formes, elle la redemande pour d'autres ouvrages. Cette recrue continuelle du genre humain, je veux dire les enfants qui naissent, à mesure qu'ils croissent et qu'ils s'avancent, semblent nous pousser de l'épaule et nous dire : Retirez-vous, c'est maintenant notre tour. Ainsi, comme nous en voyons passer d'autres devant nous, d'autres nous verront passer, qui doivent à leurs successeurs le même spectacle. Ô Dieu ! encore une fois, qu'est-ce que de nous ? Si je jette la vue devant moi, quel espace infini où je ne suis pas ! Si je la retourne en arrière, quelle suite effroyable où je ne suis plus, et que j'occupe peu de place dans cet abîme immense du temps ! Je ne suis rien ; un si petit intervalle n'est pas capable de me distinguer du néant. On ne m'a envoyé que pour faire nombre, encore n'avait-on que faire de moi ; et la pièce n'en aurait pas été moins jouée, quand je serais demeuré derrière le théâtre.

Encore si nous voulons discuter les choses dans une considération plus subtile, ce n'est pas toute l'étendue de notre vie qui nous distingue du néant ; et vous savez, chrétiens, qu'il n'y jamais qu'un moment qui nous en sépare. Maintenant nous en tenons un ; maintenant il périt ; et avec lui nous péririons tous, si promptement et sans perdre temps nous n'en saisissions un autre semblable, jusqu'à ce qu'enfin il en viendra un auquel nous ne pourrons arriver, quelque effort que nous fassions pour nous y étendre ; et alors nous tomberons tout à coup, manque de soutien. Ô fragile appui de notre être ! Ô fondement ruineux de notre substance ! \textit{In imagine pertransit homo} : ah ! l'homme passe vraiment de même qu'une ombre ou de même qu'une image en figure ; et comme lui-même n'est rien de solide, il ne poursuit aussi que des choses vaines, l'image du bien, et non le bien même. Que la place est petite que nous occupons en ce monde : si petite certainement et si peu considérable, que je doute quelquefois avec Arnobe si je dors ou si je veille. Je ne sais si ce que j'appelle veiller n'est peut-être pas une partie un peu plus animée d'un sommeil profond ; et si je vois des choses réelles, ou si je suis seulement troublé par des fantaisies et par de vains simulacres. \textit{Præterit figura hujus mundi} : \frquote{La figure de ce monde passe, et ma substance n'est rien devant Dieu :} \textit{Et substantia mea tanquam nihilum ante te.}

\begin{center}
\textsc{second point}
\end{center}

N'en doutons pas, chrétiens, quoique nous soyons relégués dans cette dernière partie de l'univers qui est le théâtre des changements et l'empire de la mort ; bien plus, quoiqu'elle nous soit inhérente et que nous la portions dans notre sein, toutefois au milieu de cette matière et à travers l'obscurité de nos connaissances qui vient des préjugés de nos sens, si nous savons rentrer en nous-mêmes, nous y trouverons quelque principe qui montre bien par sa vigueur son origine céleste, et qui n'appréhende pas la corruption. [...]\newline

\begin{flushright}
\large
\textsc{bossuet}, \textit{Sermon sur la mort}.
\end{flushright}
\end{document}
