\documentclass[a4paper,14pt,french]{extarticle}

\usepackage{geometry}
\geometry{margin=24mm}

\usepackage{fontspec}
\setmainfont{EBGaramond}
[
  Extension      = .otf ,
  UprightFont    = *-Regular,
  ItalicFont     = *-Italic,
  BoldFont       = *-Bold,
  BoldItalicFont = *-BoldItalic,
  Numbers        = Lowercase,
  Ligatures      = Discretionary,
  Style          = Swash
]
% % https://texnique.fr/osqa/questions/8423/lualatex-et-ligatures

\usepackage{babel}
\usepackage{microtype}

\begin{document}

\begin{center}
\Large Morceaux choisis des \textit{Pensées} de Pascal.
\end{center}

\vspace{1cm}
\centerline{\rule{0.3333\linewidth}{.2pt}}
\vspace{1cm}

\section*{Âme.}
\begin{quotation}
Qu'est-ce qui sent du plaisir en nous ? est-ce la main ? est-ce le
bras ? est-ce la chair ? est-ce le sang ? On verra qu'il faut que
ce soit quelque chose d'immatériel\footnote{Toute pensée prouve l'existence de l'âme. Même la sensation,
qui est aussi une pensée au sens large du terme. Voir
comment Descartes définit la pensée, dans les \emph{Méditations métaphysiques} : \og Mais qu'est donc que je suis ? Une chose qui
pense. Qu'est-ce qu'une chose qui pense ? C'est-à-dire une chose qui doute, qui conçoit, qui
affirme, qui nie, qui veut, qui ne veut pas, qui imagine aussi, et
qui sent.\fg}.
\end{quotation}

\section*{Confession.}
\begin{quotation}
La religion catholique n'oblige pas à découvrir ses péchés indifféremment
à tout le monde : elle souffre qu'on demeure caché à tous les autres
hommes ; mais elle en excepte un seul, à qui elle commande de découvrir
le fond de son cœur, et de se faire voir tel qu'on est. Il n'y a que
ce seul homme au monde qu'elle nous ordonne de désabuser, et elle
l'oblige à un secret inviolable, qui fait que cette connaissance est
dans lui comme si elle n'y était pas. Peut-on s'imaginer rien de plus
charitable et de plus doux ? Et néanmoins la corruption de l'homme
est telle, qu'il trouve encore de la dureté dans cette loi ; et c'est
une des principales raisons qui a fait révolter contre l'Église une
grande partie de l'Europe.
\end{quotation}

\section*{Descartes.}
\begin{quotation}
Descartes inutile et incertain\footnote{À quoi Pascal pense-t-il en écrivant cela ? Probablement aux sciences
de la nature, à la physique, à laquelle le nom de Descartes est associé
dans l'esprit du lecteur du \textsc{\romannumeral 17}\ieme~siècle. Voir Molière, \emph{Les
Femmes savantes,} acte III, scène 2.

Mais il est possible également que Pascal pense aux preuves cartésiennes
de l'existence de Dieu. Voir plus loin l'article \emph{Preuves}.}.
\end{quotation}

\section*{Divertissement.}
\begin{quotation}
Les hommes s'occupent à suivre une balle et un lièvre ; c'est le plaisir
même des rois.
\end{quotation}
\medskip{}

\begin{quotation}
Quelque condition qu'on se figure, si l'on assemble tous les biens
qui peuvent nous appartenir, la royauté est le plus beau poste du
monde, et cependant qu'on s'en imagine, accompagné de toutes les satisfactions
qui peuvent le toucher, s'il est sans divertissement, et qu'on le
laisse considérer et faire réflexion sur ce qu'il est, cette félicité
languissante ne le soutiendra point, il tombera par nécessité dans
les vues qui le menacent, des révoltes qui peuvent arriver, et enfin
de la mort et des maladies, qui sont inévitables ; de sorte que, s'il
est sans ce qu'on appelle divertissement, le voilà malheureux, et
plus malheureux que le moindre de ses sujets, qui joue et qui se divertit.
\end{quotation}
\medskip{}

\begin{quotation}
Les hommes n'ayant pu guérir la mort, la misère, l'ignorance, ils
se sont avisés, pour se rendre heureux, de n'y point penser.
\end{quotation}
\medskip{}

\begin{quotation}
La seule chose qui nous console de nos misères est le divertissement,
et cependant c'est la plus grande de nos misères.
\end{quotation}
\medskip{}

\begin{quotation}
Un homme dans un cachot, ne sachant si son arrêt est donné, n'ayant
plus qu'une heure pour l'apprendre, cette heure suffisant, s'il sait
qu'il est donné, pour le faire révoquer, il est contre nature qu'il
emploie cette heure-là, non à s'informer si l'arrêt est donné, mais
à jouer au piquet.
\end{quotation}
\medskip{}

\begin{quotation}
Ils s'imaginent que s'ils avaient obtenu
cette charge, ils se reposeraient ensuite avec plaisir et ne sentent
pas la nature insatiable de la cupidité. Ils croient chercher sincèrement
le repos et ne cherchent en effet que l'agitation.
Ils ont un instinct secret qui les porte à chercher le divertissement
et l'occupation au dehors, qui vient du ressentiment
de leurs misères continuelles. Et ils ont un autre instinct secret
qui reste de la grandeur de notre première nature, qui leur fait connaître
que le bonheur n'est en effet que dans le repos et
non pas dans le tumulte. Et de ces deux instincts contraires il se
forme en eux un projet confus qui se cache à leur vue dans le fond
de leur âme, qui les porte à tendre au repos par l'agitation
et à se figurer toujours que la satisfaction qu'ils
n'ont point leur arrivera si en surmontant quelques
difficultés qu'ils envisagent ils peuvent s'ouvrir
par là la porte au repos. Ainsi s'écoule toute la vie
; on cherche le repos en combattant quelques obstacles et si on les
a surmontés le repos devient insupportable, car ou l'on pense aux
misères qu'on a, ou à celles qui nous menacent.
\end{quotation}

\section*{Homme.}
\begin{quotation}
Qui ne croirait, à nous voir composer toutes choses d'esprit et de
corps, que ce mélange-là nous serait très compréhensible ? C'est néanmoins
la chose qu'on comprend le moins. L'homme est à lui-même le plus prodigieux
objet de la nature ; car il ne peut concevoir ce que c'est que corps,
et encore moins ce que c'est qu'esprit, et moins qu'aucune chose comme
un corps peut être uni avec un esprit. C'est là le comble de ses difficultés,
et cependant c'est son propre être : \emph{Modus quo corporibus adhaerent
spiritus comprehendi ab hominibus non potest, et hoc tamen homo est.}
\end{quotation}
\medskip{}

\begin{quotation}
Peu de chose nous console, parce que peu de chose nous afflige.
\end{quotation}
\medskip{}

\begin{quotation}
L'homme n'est ni ange ni bête, et le malheur veut que qui veut faire
l'ange fait la bête.
\end{quotation}

\section*{Inconstance.}
\begin{quotation}
Le sentiment de la fausseté des plaisirs présents et l'ignorance de
la vanité des plaisirs absents causent l'inconstance.
\end{quotation}

\section*{Jésus-Christ.}
\begin{quotation}
\emph{Console-toi, tu ne me chercherais pas, si tu ne m'avais trouvé.}
\end{quotation}

\section*{Justice.}
\begin{quotation}
Plaisante justice qu'une rivière borne ! Vérité au-deçà des Pyrénées,
erreur au-delà.
\end{quotation}

\section*{Montaigne.}
\begin{quotation}
Il inspire une nonchalance du salut, sans crainte et sans repentir.
Son livre n'étant pas fait pour porter à la piété, il n'y était pas
obligé : mais on est toujours obligé de n'en point détourner. On peut
excuser ses sentiments un peu libres et voluptueux en quelques rencontres
de la vie ; mais on ne peut excuser ses sentiments tout païens sur
la mort ; car il faut renoncer à toute piété, si on ne veut au moins
mourir chrétiennement : or, il ne pense qu'à mourir lâchement et mollement
par tout son livre.
\end{quotation}

\section*{Pensée.}
\begin{quotation}
En écrivant ma pensée, elle m'échappe quelquefois ; mais cela me fait
me souvenir de ma faiblesse, que j'oublie à toute heure ; ce qui m'instruit
autant que ma pensée oubliée, car je ne tends qu'à connaître mon néant.
\end{quotation}
\medskip{}

\begin{quotation}
Pensée fait la grandeur de l'homme.
\end{quotation}
\medskip{}

\begin{quotation}
Je puis bien concevoir un homme sans mains, pieds, tête (car ce n'est
que l'expérience qui nous apprend que la tête est plus nécessaire
que les pieds), mais je ne puis concevoir l'homme sans pensée : ce
serait une pierre ou une brute.
\end{quotation}

\section*{Philosophie.}
\begin{quotation}
Se moquer de la philosophie, c'est vraiment philosopher.
\end{quotation}

\section*{Preuves.}
\begin{quotation}
Les preuves de Dieu métaphysiques sont si éloignées du raisonnement
des hommes, et si impliquées, qu'elles frappent peu ; et, quand cela
servirait à quelques uns, cela ne servirait que pendant l'instant
qu'ils voient cette démonstration, mais, une heure après, ils craignent
de s'être trompés.
\end{quotation}

\section*{Raison.}
\begin{quotation}
La dernière démarche de la raison est de reconnaître qu'il y a une
infinité de choses qui la surpassent ; elle n'est que faible, si elle
ne va jusqu'à connaître cela.
\end{quotation}

\begin{quotation}
L'esprit de ce souverain juge du monde n'est pas si indépendant, qu'il
ne soit sujet à être troublé par le premier tintamarre qui se fait
autour de lui. Il ne faut pas le bruit d'un canon pour empêcher ses
pensées : il ne faut que le bruit d'une girouette ou d'une poulie.
Ne vous étonnez pas s'il ne raisonne pas bien à présent : une mouche
bourdonne à ses oreilles ; c'en est assez pour le rendre incapable
de bon conseil. Si vous voulez qu'il puisse trouver la vérité, chassez
cet animal qui tient sa raison en échec et trouble cette puissante
intelligence qui gouverne les villes et les royaumes.
\end{quotation}
\medskip{}

\begin{quotation}
Le plus grand philosophe du monde, sur une planche plus large qu'il
ne faut, s'il y a au-dessous un précipice, quoique sa raison le convainque
de sa sûreté, son imagination prévaudra. Plusieurs n'en sauraient
soutenir la pensée sans pâlir et suer.
\end{quotation}
\medskip{}

\section*{Religion.}
\begin{quotation}
J'aurais bien plus de peur de me tromper, et de trouver que la religion
chrétienne soit vraie, que non pas de me tromper en la croyant vraie\footnote{C'est le fameux pari de Pascal : il y a moins à perdre en pariant
que la religion est vraie, qu'en pariant qu'elle est fausse.}.
\end{quotation}

\section*{Sciences.}
\begin{quotation}
Les sciences ont deux extrémités qui se touchent. La première est
la pure ignorance naturelle où se trouvent tous les hommes en naissant.
L'autre extrémité est celle où arrivent les grandes
âmes, qui, ayant parcouru tout ce que les hommes peuvent savoir, trouvent
qu'ils ne savent rien, et se rencontrent en cette même
ignorance d'où ils étaient partis ; mais c'est
une ignorance savante qui se connaît. Ceux d'entre
eux, qui sont sortis de l'ignorance naturelle, et n'ont
pu arriver à l'autre, ont quelque teinture de cette
science suffisante, et font les entendus. Ceux-là troublent le monde,
et jugent mal de tout.
\end{quotation}

\section*{Vanité.}
\begin{quotation}
La vanité est si ancrée dans le c½ur de l'homme, qu'un soldat, un
goujat, un cuisinier, un crocheteur se vante et veut avoir ses admirateurs
; et les philosophes mêmes en veulent ; et ceux qui écrivent contre
veulent avoir la gloire d'avoir bien écrit ; et ceux qui les lisent
veulent avoir la gloire de les avoir lus ; et moi, qui écris ceci,
ai peut-être cette envie ; et peut-être que ceux qui le liront...
\end{quotation}
\medskip{}

\begin{quotation}
La douceur de la gloire est si grande, qu'en quelque objet qu'on l'attache,
même à la mort, on l'aime.
\end{quotation}

\section*{Vérité.}
\begin{quotation}
Contradiction est une mauvaise marque de vérité : plusieurs choses
certaines sont contredites ; plusieurs fausses passent sans contradiction.
Ni la contradiction n'est marque de fausseté, ni l'incontradiction
n'est marque de vérité.
\end{quotation}

\section*{Vie.}
\begin{quotation}
Entre nous, et l'enfer ou le ciel, il n'y a que la vie entre deux,
qui est la chose du monde la plus fragile.
\end{quotation}

\end{document}
