\documentclass[a4paper,14pt,french]{extarticle}

\usepackage{geometry}
\geometry{margin=24mm}

\usepackage{fontspec}
\setmainfont{EBGaramond}
[
  Extension      = .otf ,
  UprightFont    = *-Regular,
  ItalicFont     = *-Italic,
  BoldFont       = *-Bold,
  BoldItalicFont = *-BoldItalic,
  Numbers        = Lowercase,
  Ligatures      = Discretionary,
  Style          = Swash
]
% % https://texnique.fr/osqa/questions/8423/lualatex-et-ligatures

\usepackage{babel}
\usepackage{microtype}

\begin{document}

\begin{center}
\large Nous haïssons la vérité.
\end{center}

La nature de l'amour-propre et de ce \textit{moi} humain est de n'aimer que soi et de ne considérer que soi. Mais que fera-t-il ? Il ne saurait empêcher que cet objet qu'il aime ne soit plein de défauts et de misères : il veut être grand, il se voit petit ; il veut être heureux, et il se voit misérable ; il veut être parfait, et il se voit plein d'imperfections ; il veut être l'objet de l'amour et de l'estime des hommes, et il voit que ses défauts ne méritent que leur aversion et leur mépris. Cet embarras où il se trouve produit en lui la plus injuste et la plus criminelle passion qu'il soit possible de s'imaginer ; car il conçoit une haine mortelle contre cette vérité qui le reprend, et qui le convainc de ses défauts. Il désirerait de l'anéantir, et, ne pouvant la détruire en elle-même il la détruit, autant qu'il peut, dans sa connaissance et dans celle des autres ; c'est-à-dire qu'il met tout son soin à couvrir ses défauts et aux autres et à soi-même, et qu'il ne peut souffrir qu'on les lui fasse voir ni qu'on les voie.

C'est sans doute un mal que d'être plein de défauts ; mais c'est encore un plus grand mal que d'en être plein et de ne les vouloir pas reconnaître, puisque c'est y ajouter encore celui d'une illusion volontaire. Nous ne voulons pas que les autres nous trompent ; nous ne trouvons pas juste qu'ils veuillent être estimés de nous plus qu'ils ne méritent : il n'est donc pas juste aussi que nous les trompions et que nous voulions qu'ils nous estiment plus que nous ne méritons.

Ainsi, lorsqu'ils ne découvrent que des imperfections et des vices que nous avons en effet, il est visible qu'ils ne nous font point de tort, puisque ce ne sont pas eux qui en sont cause, et qu'ils nous font un bien, puisqu'ils nous aident à nous délivrer d'un mal, qui est l'ignorance de ces imperfections. Nous ne devons pas être fâchés qu'ils les connaissent, et qu'ils nous méprisent : étant juste et qu'ils nous connaissent pour ce que nous sommes, et qu'ils nous méprisent, si nous sommes méprisables.

Voilà les sentiments qui naîtraient d'un cœur qui serait plein d'équité et de justice. Que devons-nous donc dire du nôtre, en y voyant une disposition toute contraire ? Car n'est-il pas vrai que nous haïssons la vérité et ceux qui nous la disent, et que nous aimons qu'ils se trompent à notre avantage, et que nous voulons être estimés d'eux autres que nous ne sommes en effet ?

En voici une preuve qui me fait horreur. La religion catholique n'oblige pas à découvrir ses péchés indifféremment à tout le monde : elle souffre qu'on demeure caché à tous les autres hommes ; mais elle en excepte un seul, à qui elle commande de découvrir le fond de son cœur, et de se faire voir tel qu'on est. Il n'y a que ce seul homme au monde qu'elle nous ordonne de désabuser, et elle l'oblige à un secret inviolable, qui fait que cette connaissance est dans lui comme si elle n'y était pas. Peut-on s'imaginer rien de plus charitable et de plus doux ? Et néanmoins la corruption de l'homme est telle, qu'il trouve encore de la dureté dans cette loi ; et c'est une des principales raisons qui a fait révolter contre l'Église une grande partie de l'Europe.

Que le cœur de l‘homme est injuste et déraisonnable, pour trouver mauvais qu'on l'oblige à faire à l'égard d'un homme ce qu'il serait juste, en quelque sorte, qu'il fît à l'égard de tous les hommes ! Car est-il juste que nous les trompions ?

Il y a différents degrés dans cette aversion pour la vérité ; mais on peut dire qu'elle est dans tous en quelque degré, parce qu'elle est inséparable de l'amour-propre. C'est cette mauvaise délicatesse qui oblige ceux qui sont dans la nécessité de reprendre les autres, de choisir tant de détours et de tempéraments pour éviter de les choquer. Il faut qu'ils diminuent nos défauts, qu'ils fassent semblant de les excuser, qu'ils y mêlent des louanges et des témoignages d'affection et d'estime. Avec tout cela, cette médecine ne laisse pas d'être amère à l'amour-propre. Il en prend le moins qu'il peut, et toujours avec dégoût, et souvent même avec un secret dépit contre ceux qui la lui présentent.

Il arrive de là que, si on a quelque intérêt d'être aimé de nous, on s'éloigne de nous rendre un office qu'on sait nous être désagréable ; on nous traite comme nous voulons être traités : nous haïssons la vérité, on nous la cache ; nous voulons être flattés, on nous flatte ; nous aimons à être trompés, on nous trompe.

C'est ce qui fait que chaque degré de bonne fortune qui nous élève dans le monde nous éloigne davantage de la vérité, parce qu'on appréhende plus de blesser ceux dont l'affection est plus utile et l'aversion plus dangereuse. Un prince sera la fable de toute l'Europe, et lui seul n'en saura rien. Je ne m'en étonne pas : dire la vérité est utile à celui à qui on la dit, mais désavantageux à ceux qui la disent, parce qu'ils se font haïr. Or, ceux qui vivent avec les princes aiment mieux leurs intérêts que celui du prince qu'ils servent ; et ainsi, ils n'ont garde de lui procurer un avantage en se nuisant à eux-mêmes.

Ce malheur est sans doute plus grand et plus ordinaire dans les plus grandes fortunes ; mais les moindres n'en sont pas exemptes, parce qu'il y a toujours quelque intérêt à se faire aimer des hommes. Ainsi la vie humaine n'est qu'une illusion perpétuelle ; on ne fait que s'entre-tromper et s'entre-flatter. Personne ne parle de nous en notre présence comme il en parle en notre absence. L'union qui est entre les hommes n'est fondée que sur cette mutuelle tromperie ; et peu d'amitiés subsisteraient, si chacun savait ce que son ami dit de lui lorsqu'il n'y est pas, quoiqu'il en parle alors sincèrement et sans passion.

L'homme n'est donc que déguisement, que mensonge et hypocrisie, et en soi-même et à l'égard des autres. Il ne veut donc pas qu'on lui dise la vérité. Il évite de la dire aux autres ; et toutes ces dispositions, si éloignées de la justice et de la raison, ont une racine naturelle dans son cœur.

Je mets en fait que si tous les hommes savaient ce qu'ils disent les uns des autres, il n'y aurait pas quatre amis dans le monde. Cela paraît par les querelles que causent les rapports indiscrets qu'on en fait quelquefois.

\begin{flushright}
\large
\textsc{pascal}, \textit{Pensées}.
\end{flushright}
\end{document}
