\documentclass[a4paper,14pt,french]{extarticle}

\usepackage{geometry}
\geometry{margin=24mm}

\usepackage{fontspec}
\setmainfont{EBGaramond}
[
  Extension      = .otf ,
  UprightFont    = *-Regular,
  ItalicFont     = *-Italic,
  BoldFont       = *-Bold,
  BoldItalicFont = *-BoldItalic,
  Numbers        = Lowercase,
  Ligatures      = Discretionary,
  Style          = Swash
]
% % https://texnique.fr/osqa/questions/8423/lualatex-et-ligatures

\usepackage{babel}
\usepackage{microtype}

\begin{document}

\begin{center}
\large Sur l'indifférence.
\end{center}

L'immortalité de l'âme est une chose qui nous importe si fort, qui nous touche si profondément, qu'il faut avoir perdu tout sentiment pour être dans l'indifférence de savoir ce qui en est. Toutes nos actions et nos pensées doivent prendre des routes si différentes, selon qu'il y aura des biens éternels à espérer ou non, qu'il est impossible de faire une démarche avec sens et jugement, qu'en la réglant par la vue de ce point qui doit être notre dernier objet. Ainsi notre premier intérêt et notre premier devoir est de nous éclaircir sur ce sujet, d'où dépend toute notre conduite. Et c'est pourquoi, entre ceux qui n'en sont pas persuadés, je fais une extrême différence de ceux qui travaillent de toutes leurs forces à s'en instruire, à ceux qui vivent sans s'en mettre en peine et sans y penser.

Je ne puis avoir que de la compassion pour ceux qui gémissent sincèrement dans ce doute, qui le regardent comme le dernier des malheurs, et qui n'épargnant rien pour en sortir, font de cette recherche leurs principales et leurs plus sérieuses occupations. Mais pour ceux qui passent leur vie sans penser à cette dernière fin de la vie et qui, par cette seule raison qu'ils ne trouvent pas en eux-mêmes les lumières qui les persuadent, négligent de les chercher ailleurs et d'examiner à fond si cette opinion est de celles que le peuple reçoit par une simplicité crédule, ou de celles qui quoique obscures d'elles-mêmes ont néanmoins un fondement très-solide et inébranlable ; je les considère d'une manière toute différente. Cette négligence en une affaire où il s'agit d'eux-mêmes, de leur éternité, de leur tout, m'irrite plus qu'elle ne m'attendrit ; elle m'étonne et m'épouvante : c'est un monstre pour moi...

Il ne faut pas avoir l'âme fort élevée pour comprendre qu'il n'y a point ici de satisfaction véritable et solide ; que tous nos plaisirs ne sont que vanité ; que nos maux sont infinis, et qu'enfin la mort qui nous menace à chaque instant doit infailliblement nous mettre dans peu d'années dans l'horrible nécessité d'être éternellement ou anéantis ou malheureux. Il n'y a rien de plus réel que cela, ni de plus terrible. Faisons tant que nous voudrons les braves : voilà la fin qui attend la plus belle vie du monde.

Qu'on fasse réflexion là-dessus et qu'on dise ensuite s'il n'est pas indubitable qu'il n'y a de bien en cette vie qu'en l'espérance d'une autre vie ; qu'on n'est heureux qu'à mesure qu'on s'en approche et que comme il n'y aura plus de malheurs pour ceux qui avaient une entière assurance de l'éternité, il n'y a point aussi de bonheur pour ceux qui n'en ont aucune lumière.

C'est donc assurément un grand mal que d'être dans ce doute ; mais c'est au moins un devoir indispensable de chercher quand on est dans ce doute, et ainsi celui qui doute et qui ne cherche pas est tout ensemble et bien malheureux et bien injuste. Que s'il est avec cela tranquille et satisfait, qu'il en fasse profession, et enfin qu'il en fasse vanité, et que ce soit de cet état même qu'il fasse le sujet de sa joie et de sa vanité, je n'ai point de termes pour qualifier une si extravagante créature.

Où peut-on prendre ces sentiments? Quel sujet de joie trouve-t-on à n'attendre plus que des misères sans ressource? Quel sujet de vanité de se voir dans des obscurités impénétrables et comment se peut-il faire que ce raisonnement-ci se passe dans un homme raisonnable :

\itshape{Je ne sais qui m'a mis au monde, ni ce que c'est que le monde, ni que moi-même. Je suis dans une ignorance terrible de toutes choses. Je ne sais ce que c'est que mon corps, que mes sens, que mon âme et cette partie même de moi qui pense ce que je dis, qui fait réflexion sur tout et sur elle-même, et ne se connaît non plus que le reste. Je vois ces effroyables espaces de l'univers qui m'enferment, et je me trouve attaché à un coin de cette vaste étendue, sans que je sache pourquoi je suis plutôt placé en ce lieu qu'en un autre, ni pourquoi ce peu de temps qui m'est donné à vivre m'est assigné à ce point plutôt qu'en un autre de toute l'éternité qui m'a précédé, et de toute celle qui me suit.

Je ne vois que des infinités de toutes parts, qui m'enferment comme un atome, et comme une ombre qui ne dure qu'un instant sans retour. Tout ce que je connais, est que je dois bientôt mourir; mais ce que j'ignore le plus, est cette mort même que je ne saurais éviter.

Comme je ne sais d'où je viens, aussi je ne sais où je vais; et je sais seulement qu'en sortant de ce monde je tombe pour jamais, ou dans le néant, ou dans les mains d'un Dieu irrité, sans savoir à laquelle de ces deux conditions je dois être éternellement en partage. Voilà mon état, plein de misère, de faiblesse, d'obscurité. Et de tout cela je conclus que je dois donc passer tous les jours de ma vie sans songer à chercher ce qui doit m'arriver. Peut-être que je pourrais trouver quelque éclaircissement dans mes doutes ; mais je n'en veux pas prendre la peine, ni faire un pas pour le chercher; et après, en traitant avec mépris ceux qui se travailleront de ce soin, je veux aller sans prévoyance et sans crainte tenter un si grand événement, et me laisser mollement conduire à la mort, dans l'incertitude de l'éternité de ma condition future.}

Qui souhaiterait avoir pour ami un homme qui discourt de cette manière ? Qui le choisirait entre les autres pour lui communiquer ses affaires ? Qui aurait recours à lui dans ses afflictions ? Et enfin à quel usage de la vie le pourrait-on destiner ?

En vérité, il est glorieux à la religion d'avoir pour ennemis des hommes si déraisonnables ; et leur opposition lui est si peu dangereuse, qu'elle sert au contraire à l'établissement de ses principales vérités. Car la foi chrétienne ne va principalement qu'à établir ces deux choses : la corruption de la nature et la rédemption de Jésus-Christ. Or, s'ils ne servent pas à montrer la vérité de la rédemption par la sainteté de leurs mœurs, ils servent au moins admirablement à montrer la corruption de la nature par des sentiments si dénaturés.

Rien n'est si important à l'homme que son état ; rien ne lui est si redoutable que l'éternité. Et ainsi, qu'il se trouve des hommes indifférents à la perte de leur être et au péril d'une éternité de misères, cela n'est point naturel. Ils sont tout autres à l'égard de toutes les autres choses : ils craignent jusqu'aux plus légères, ils les prévoient, ils les sentent ; et ce même homme qui passe tant de jours et de nuits dans la rage et dans le désespoir pour la perte d'une charge, ou pour quelque offense imaginaire à son honneur, c'est celui-là même qui sait qu'il va tout perdre par la mort, sans inquiétude et sans émotion. C'est une chose monstrueuse de voir dans un même cœur et en même temps cette sensibilité pour les moindres choses et cette étrange insensibilité pour les plus grandes. C'est un enchantement incompréhensible, et un assoupissement surnaturel, qui marque une force toute-puissante qui le cause.

Il faut qu'il y ait un étrange renversement dans la nature de l'homme pour faire gloire d'être dans cet état dans lequel il semble incroyable qu'une seule personne puisse être. Cependant l'expérience m'en fait voir en si grand nombre que cela serait surprenant, si nous ne savions que la plupart de ceux qui s'en mêlent se contrefont et ne sont pas tels en effet. Ce sont des gens qui ont ouï dire que les belles manières du monde consistent à faire ainsi l'emporté. C'est ce qu'ils appellent avoir secoué le joug, et qu'ils essayent d'imiter. Mais il ne serait pas difficile de leur faire entendre combien ils s'abusent en cherchant par là de l'estime. Ce n'est pas le moyen d'en acquérir, je dis même parmi les personnes du monde qui jugent sainement des choses, et qui savent que la seule voie d'y réussir est de se faire paraître honnête, fidèle, judicieux, et capable de servir utilement son ami ; parce que les hommes n'aiment naturellement que ce qui leur peut être utile. Or, quel avantage y a-t-il pour nous à ouïr dire à un homme, qui nous dit qu'il a donc secoué le joug, qu'il ne croit pas qu'il y ait un Dieu qui veille sur ses actions; qu'il se considère comme seul maître de sa conduite et qu'il ne pense en rendre compte qu'à soi-même? Pense-t-il nous avoir portés par là à avoir désormais bien de la confiance en lui, et à en attendre des consolations, des conseils et des secours dans tous les besoins de la vie ? Prétendent-ils nous avoir bien réjouis, de nous dire qu'ils tiennent que notre âme n'est qu'un peu de vent et de fumée, et encore de nous le dire d'un ton de voix fier et content ? Est-ce donc une chose à dire gaiement ? et n'est-ce pas une chose à dire tristement au contraire, comme la chose du monde la plus triste ?

S'ils y pensaient sérieusement, ils verraient que cela est si mal pris, si contraire au bon sens, si opposé à l'honnêteté et si éloigné en toute manière de ce bon air qu'ils cherchent, qu'ils seraient plutôt capables de redresser que de corrompre ceux qui auraient quelque inclination à les suivre. Et, en effet, faites-leur rendre compte de leurs sentiments et des raisons qu'ils ont de douter de la religion : ils diront des choses si faibles et si basses, qu'ils vous persuaderont du contraire. C'était ce que leur disait un jour fort à propos une personne : Si vous continuez à discourir de la sorte, leur disait-il, en vérité vous me convertirez. Et il avait raison ; car qui n'aurait horreur de se voir dans des sentiments où l'on a pour compagnons des personnes si méprisables ?

Ainsi ceux qui ne font que feindre ces sentiments seraient bien malheureux de contraindre leur naturel pour se rendre les plus impertinents des hommes. S'ils sont fâchés dans le fond de leur cœur de n'avoir pas plus de lumière, qu'ils ne le dissimulent pas : cette déclaration ne sera point honteuse. Il n'y a de honte qu'à n'en point avoir. Rien n'accuse davantage une extrême faiblesse d'esprit que de ne pas connaître quel est le malheur d'un homme sans Dieu; rien ne marque davantage une mauvaise disposition du cœur que de ne pas souhaiter la vérité des promesses éternelles ; rien n'est plus lâche que de faire le brave contre Dieu. Qu'ils laissent donc ces impiétés à ceux qui sont assez mal nés pour en être véritablement capables ; qu'ils soient au moins honnêtes gens, s'ils ne peuvent être chrétiens et qu'ils reconnaissent enfin qu'il n'y a que deux sortes de personnes qu'on puisse appeler raisonnables : ou ceux qui servent Dieu de tout leur cœur parce qu'ils le connaissent, ou ceux qui le cherchent de tout leur cœur parce qu'ils ne le connaissent pas.

Mais pour ceux qui vivent sans le connaître et sans le chercher, ils se jugent eux-mêmes si peu dignes de leur soin, qu'ils ne sont pas dignes du soin des autres ; et il faut avoir toute la charité de la religion qu'ils méprisent pour ne les pas mépriser jusqu'à les abandonner dans leur folie.

\begin{flushright}
\large
\textsc{pascal}, \textit{Pensées}.
\end{flushright}
\end{document}
