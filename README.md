# Textes

Choix de textes français classés par auteur et par œuvre.

## Utilisation

Les fichiers sont écrits en [LaTeX](https://doc.ubuntu-fr.org/latex), et sont faits pour être convertis en documents PDF au moyen de [LuaLaTeX](https://doc.ubuntu-fr.org/lualatex).

Chaque dossier contient un *Makefile* servant à générer les documents PDF. Pour ce faire il suffit d'ouvrir un terminal et de taper `make NOM_DU_FICHIER` (sans l'extension *.tex*), ou `make all` pour générer d'un coup tous les documents du dossier.

## Table des auteurs et des œuvres

### Bardèche (Maurice)

- [Nuremberg ou la terre promise](bardeche/nuremberg)

### Bloy (Léon)

- [Je m'accuse](bloy/zola)

### Bonald (Louis de)

- [Du Divorce](bonald/divorce)
- [Théorie du pouvoir politique et religieux](bonald/theorie)

### Bossuet (Jacques-Bénigne)

- [Oraison funèbre de Henriette de France](bossuet/oraison-funebre-henriette-france)
- [Sermon sur la mort](bossuet/mort)

### Céline (Louis-Ferdinand)

- [Bagatelles pour un massacre](celine/bagatelles)
- [L'École des cadavres](celine/ecole)
- [Les Beaux Draps](celine/draps)

### Descartes (René)

- [Méditations métaphysiques](descartes/meditations)

### Leibniz (Gottfried Wilhelm)

- [Nouveaux Essais sur l'entendement humain](leibniz/nouveaux-essais)

### Nietzsche (Friedrich)

- [Ainsi parlait Zarathoustra](nietzsche/zarathoustra)

### Pascal (Blaise)

- [Entretien avec M. de Saci](pascal/entretien)

### Platon

- [La République](platon/republique)
