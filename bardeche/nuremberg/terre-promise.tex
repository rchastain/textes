\documentclass[12pt,a4paper]{article}

\usepackage{fontspec}
\usepackage[french]{babel}
\usepackage{microtype}
\usepackage[a4paper,margin=24mm]{geometry}

%~ \setmainfont[
    %~ Path           = libre-baskerville/,
    %~ Extension      = .otf,
    %~ Ligatures      = TeX,
    %~ BoldFont       = LibreBaskerville-Bold,
    %~ ItalicFont     = LibreBaskerville-Italic
%~ ]{LibreBaskerville-Regular}

%~ https://www.fontsquirrel.com/fonts/libre-baskerville

%~ \setmainfont{LibreBaskerville}

\setmainfont{EBGaramond}
[
  Extension      = .otf ,
  UprightFont    = *-Regular,
  ItalicFont     = *-Italic,
  BoldFont       = *-Bold,
  BoldItalicFont = *-BoldItalic,
  Numbers        = Lowercase,
  Ligatures      = Discretionary,
  Style          = Swash
]
% https://texnique.fr/osqa/questions/8423/lualatex-et-ligatures

\setlength{\columnsep}{6mm} 

\usepackage{lastpage}
\usepackage{fancyhdr}
\pagestyle{fancy}
\fancyhf{}
\fancyfoot[C]{\normalfont\sffamily\thepage \hspace{1pt} / \pageref{LastPage}}
\renewcommand{\headrulewidth}[0]{0pt}

\begin{document}

\begin{center}
\Large
La terre promise.
\end{center}

\thispagestyle{fancy}

Le point final est atteint ici. Les différences nationales seront peu à peu laminées. La loi internationale s'installera d'autant mieux que la loi indigène n'aura plus de défenseurs. Les gérances nationales que nous décrivions tout à l'heure prennent dans cette perspective leur véritable signification : les États ne seront plus que les arrondissements administratifs d'un seul Empire. Et d'un bout à l'autre du monde, dans des villes parfaitement pareilles puisqu'elles auront été reconstruites après quelques bombardements, vivra sous des lois semblables une population bâtarde, race d'esclaves indéfinissable et morne, sans génie, sans instinct, sans voix. L'homme déshydraté régnera dans un monde hygiénique. D'immenses bazars résonnants de pick-up symboliseront cette race à prix unique. Des trottoirs roulants parcourront les rues. Ils transporteront chaque matin à leur travail d'esclave la longue file des hommes sans visage et ils les ramèneront le soir. Et ce sera la terre promise. Ils ne sauront plus, les usagers du trottoir roulant, qu'il y eut jadis une condition humaine. Ils ne sauront pas ce qu'étaient nos cités, quand elles étaient nos cités : pas plus que nous ne pouvons imaginer ce qu'étaient Gand ou Bruges au temps des échevins. Ils s'étonneront que la terre ait été belle et que nous l'ayons aimée passionnément. Eux, la conscience universelle propre, théorique, découpée en rondelles, illuminera leurs ciels. Mais ce sera la terre promise.

Et au-dessus régnera en effet la Personne Humaine, celle pour qui on a fait cette guerre, celle qui a inventé cette loi. Car enfin, on a beau dire, il y a une Personne Humaine. Ce n'est pas les Allemands de la Volga, ce n'est pas les Baltes, ce n'est pas les Chinois, ce n'est pas les Malgaches, ce n'est pas les Annamites, ce n'est pas les Tchèques, ce n'est pas les prolétaires, bien entendu. La Personne Humaine, nous savons très bien ce que c'est. Ce terme n'a toute sa signification, on peut même dire qu'il n'a de signification, au sens où l'entend le Tribunal, que s'il s'applique à un individu apatride, qui est né dans un faubourg de Cracovie, qui a souffert sous Hitler, a été déporté, n'est pas mort, a quand même été ressuscité, sous la forme d'un patriote français, belge ou luxembourgeois, sur lequel nous sommes invités à reporter tout ce qui est en nous de déférence et d'adoration. La Personne Humaine est, en outre, habituellement munie d'un passeport international, d'une autorisation d'exportation, d'une dispense d'impôt et du droit de réquisitionner les appartements. Ajoutons que la Personne Humaine ainsi définie est tout spécialement dépositaire de la conscience universelle : elle en est, pour ainsi dire, le vase d'élection. Elle possède pour cela des organes d'une sensibilité exquise qui manquent aux autres hommes : ainsi dans le pays où elle vient d'arriver, elle désigne avec sûreté les véritables patriotes et détecte à une grande distance les organismes réfractaires aux vibrations de la conscience universelle. Aussi ces précieux dons sont-ils utilisés comme il convient devant l'opinion. Toutes leurs réactions vibratiles sont précieusement enregistrées et le total de ces vibrations constitue ce qu'on appelle à un moment donné l'indignation ou l'approbation de la conscience universelle. Ce sont elles qui formulent finalement le dogme que nous avons déjà dit et qui porte pour titre : défense de la personne humaine.

Il en résulte que la défense de la personne humaine, au sens où l'entend le Tribunal, est une sorte de vérité mathématique, à peu près analogue à la règle de trois. On peut l'exprimer ainsi : \frquote{Quiconque est apatride et né à Cracovie réside au sein de la communauté universelle, et tout acte qui l'écorche ou le lèse retentit profondément au sein de la conscience humaine ; dans la mesure où votre définition spécifique vous éloigne du caractère apatride et de l'origine cracovienne, vous vous éloignez pour autant de la communauté universelle et ce qui vous lèse n'a plus qu'un retentissement correspondant dans la conscience humaine ; si vous êtes résolument hostile aux individus apatrides originaires de Cracovie, vous ne faites pas partie du tout de la communauté universelle et l'on peut entreprendre contre vous tout ce qu'on veut sans que la conscience humaine se sente le moins du monde blessée.}

Ces catéchumènes de l'Humanité nouvelle ont leurs usages qui sont sacrés. Ils ne travaillent point la terre, ils ne produisent rien, ils répugnent à l'esclavage. Ils ne se mêlent pas aux hommes du trottoir roulant, ils les comptent et les dirigent vers les tâches qui leur sont assignées. Ils ne font point la guerre, mais ils aiment à s'établir dans des boutiques brillantes de lumière où ils vendent le soir très cher à l'homme du trottoir roulant ce qu'il a fabriqué et qu'ils lui ont acheté très bon marché. Nul n'a le droit de les appeler marchands d'esclaves et pourtant les peuples au milieu desquels ils se sont établis ne travaillent que pour eux. Ils forment un ordre. C'est ce qu'ils ont de commun avec nos anciennes chevaleries. Et n'est-il pas juste qu'ils soient distingués des autres hommes, puisqu'ils sont les plus sensibles à la voix de la conscience universelle et nous fournissent le modèle sur lequel nous devons nous conformer ? Ils ont aussi leurs grands-prêtres dans des capitales lointaines. Ils vénèrent en eux les représentants de ces familles illustres qui se sont fait connaître en gagnant beaucoup d'argent et en distribuant beaucoup de publicité. Et ils se réjouissent de lire sur les armoiries de ces héros le chiffre de leurs dividendes. Mais ces puissants ont de grands soucis. Ils méditent sur la carte du monde et décident que tel pays produira désormais des oranges, et tel autre des canons. Penchés sur des graphiques, ils canalisent les millions d'esclaves du trottoir roulant et ils fixent dans leur sagesse le nombre de chemises qu'il leur sera permis de s'acheter dans l'année et le chiffre des calories qui leur seront attribuées pour vivre. Et le travail des autres hommes circule et s'inscrit sur les murs de leur cabinet comme en ces panneaux aux tubulures transparentes sur lesquels courent sans arrêt diverses sèves colorées. Ils sont les machinistes de l'univers. Qui se révolte contre eux parle contre les dieux. Ils distribuent et décident. Et leurs serviteurs, placés aux carrefours reçoivent leurs ordres avec reconnaissance, et ils indiquent sa direction à l'homme du trottoir roulant. Ainsi fonctionne le monde sans frontières, le monde où tout le monde est chez soi, et qu'ils ont appelé la terre promise.

\begin{flushright}
\large
\textsc{bardèche}, \textit{Nuremberg ou la terre promise.}
\end{flushright}
\end{document}
